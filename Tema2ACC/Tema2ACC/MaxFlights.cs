﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Tema2ACC
{
    class MaxFlights
    {
        private int nrNodes;

        public MaxFlights(int nrNodes)
        {
            this.nrNodes = nrNodes;
        }

        private bool bfs(int[,] rGraph, int s, int t, int[] parent)
        {
            bool[] visited = new bool[nrNodes];
            for (int i = 0; i < nrNodes; ++i)
                visited[i] = false;

            List<int> queue = new List<int>();
            queue.Add(s);
            visited[s] = true;
            parent[s] = -1;

            while (queue.Count != 0)
            {
                int u = queue[0];
                queue.RemoveAt(0);

                for (int v = 0; v < nrNodes; v++)
                {
                    if (visited[v] == false && rGraph[u, v] > 0)
                    {
                        queue.Add(v);
                        parent[v] = u;
                        visited[v] = true;
                    }
                }
            }
 
            return (visited[t] == true);
        }

        public void fordFulkerson(int[,] graph, int s, int t, int nrFlights, int nrCrews)
        {
            int u, v;
            int[] flights = new int[nrFlights];
            for (int i = 0; i < nrFlights; i++)
                flights[i] = -1;

            int[,] rGraph = new int[nrNodes, nrNodes];

            for (u = 0; u < nrNodes; u++)
                for (v = 0; v < nrNodes; v++)
                    rGraph[u, v] = graph[u, v];

            int[] parent = new int[nrNodes];

            int max_flow = 0; 

            while (bfs(rGraph, s, t, parent))
            {
                int path_flow = int.MaxValue;
                for (v = t; v != s; v = parent[v])
                {
                    u = parent[v];
                    path_flow = Math.Min(path_flow, rGraph[u, v]);
                }

                for (v = t; v != s; v = parent[v])
                {
                    u = parent[v];
                    rGraph[u, v] -= path_flow;
                    rGraph[v, u] += path_flow;
                }

                flights[parent[parent.Length - 1] - nrCrews - 1] = parent[parent[parent.Length - 1]];
                max_flow += path_flow;
            }

            for (int i = 0; i < flights.Length - 1; i++)
                for (int j = i + 1; j < flights.Length; j++)
                    if (flights[i] == flights[j])
                    {
                        flights[i] = parent[i + nrCrews + 1];
                        break;
                    }

            for (int i = 0; i < nrFlights; i++)
                Console.Write($"{flights[i]} ");
        }
    }
}
