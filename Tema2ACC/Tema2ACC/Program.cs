﻿using System;

namespace Tema2ACC
{
    class Program
    {
        private static int[,] graph;
        private static int nrNodes;
        private static int nrFlights;
        private static int nrCrews;
        static void Main(string[] args)
        {
            makeGraph();
            MaxFlights maxFlights = new MaxFlights(nrNodes);
            maxFlights.fordFulkerson(graph, 0, nrNodes - 1, nrFlights, nrCrews);
        }

        private static void makeGraph()
        {
            string[] line;
            System.IO.StreamReader file = new System.IO.StreamReader("file.txt");
            line = file.ReadLine().Split();
            nrFlights = int.Parse(line[0]);
            nrCrews = int.Parse(line[1]);

            int[,] matrix = new int[nrFlights, nrCrews];
            for(int i=0;i<nrFlights;i++)
            {
                line = file.ReadLine().Split();
                for(int j =0; j<nrCrews;j++)
                {
                    matrix[i, j] = int.Parse(line[j]);
                }
            }

            nrNodes = nrCrews + nrFlights + 2;
            graph = new int[nrNodes, nrNodes];

            for (int i = 0; i < nrCrews; i++)
                graph[0, i + 1] = 1;
            for (int i = 0; i < nrFlights; i++)
                graph[i + nrCrews + 1, nrNodes-1] = 1;
            for(int i=0;i<nrCrews;i++)
            {
                for(int j = 0; j < nrFlights; j++)
                {
                    if (matrix[j, i] == 1)
                    {
                        graph[i + 1, j + nrCrews + 1] = 1;
                    }
                }
            }
            //for (int i = 0; i < nrNodes; i++)
            //{
            //    for (int j = 0; j < nrNodes; j++)
            //        Console.Write($"{graph[i, j]} ");
            //    Console.WriteLine();
            //}
        }
    }
}
